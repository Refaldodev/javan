-- query insert karyawan
INSERT INTO `karyawan`(`nama`, `jenis_kelamin`, `status`, `tanggal_lahir`, `tanggal_masuk`, `Departemen`) VALUES
	('Rizki Saputra', 'L', 'Menikah', STR_TO_DATE('10/11/1980', '%m/%d/%Y'), STR_TO_DATE('1/1/2011', '%m/%d/%Y'), 1),
	('Farhan Reza', 'L', 'Belum', STR_TO_DATE('11/1/1989', '%m/%d/%Y'), STR_TO_DATE('1/1/2011', '%m/%d/%Y'), 1),
	('Riyando Adi', 'L', 'Menikah', STR_TO_DATE('1/25/1977', '%m/%d/%Y'), STR_TO_DATE('1/1/2011', '%m/%d/%Y'), 1),
	('Diego Manuel', 'L', 'Menikah', STR_TO_DATE('2/22/1983', '%m/%d/%Y'), STR_TO_DATE('9/4/2012', '%m/%d/%Y'), 2),
	('Satya Laksana', 'L', 'Menikah', STR_TO_DATE('1/12/1981', '%m/%d/%Y'), STR_TO_DATE('3/19/2011', '%m/%d/%Y'), 2),
	('Miguel Hernandez', 'L', 'Menikah', STR_TO_DATE('10/16/1994', '%m/%d/%Y'), STR_TO_DATE('6/15/2014', '%m/%d/%Y'), 2),
	('Putri Persada', 'P', 'Menikah', STR_TO_DATE('1/30/1988', '%m/%d/%Y'), STR_TO_DATE('4/14/2013', '%m/%d/%Y'), 2),
	('Alma Safira', 'P', 'Menikah', STR_TO_DATE('5/18/1991', '%m/%d/%Y'), STR_TO_DATE('9/28/2013', '%m/%d/%Y'), 3),
	('Haqi Hafiz', 'L', 'Belum', STR_TO_DATE('9/19/1995', '%m/%d/%Y'), STR_TO_DATE('3/9/2015', '%m/%d/%Y'), 3),
	('Abi Isyawara', 'L', 'Belum', STR_TO_DATE('9/19/1995', '%m/%d/%Y'), STR_TO_DATE('1/22/2012', '%m/%d/%Y'), 3),
	('Maman Kresna', 'L', 'Belum', STR_TO_DATE('6/3/1991', '%m/%d/%Y'), STR_TO_DATE('9/15/2012', '%m/%d/%Y'), 3),
	('Nadia Aulia', 'P', 'Belum', STR_TO_DATE('8/21/1993', '%m/%d/%Y'), STR_TO_DATE('5/7/2012', '%m/%d/%Y'), 4),
	('Mutiara Rezki', 'P', 'Menikah', STR_TO_DATE('3/23/1988', '%m/%d/%Y'), STR_TO_DATE('5/21/2013', '%m/%d/%Y'), 4),
	('Dani Setiawan', 'L', 'Belum', STR_TO_DATE('2/11/1986', '%m/%d/%Y'), STR_TO_DATE('11/30/2014', '%m/%d/%Y'), 4),
	('Budi Putra', 'L', 'Belum', STR_TO_DATE('10/23/1995', '%m/%d/%Y'), STR_TO_DATE('12/3/2015', '%m/%d/%Y'), 4);

-- query insert departemen
INSERT INTO `departemen`(`nama`) VALUES ('Manajemen'), ('Pengembangan Bisnis'), ('Teknisi'), ('Analis');

