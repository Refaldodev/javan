<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GanjilGenapModel extends Model
{
    public static function check($varA, $varB){
        $results = [];
        if($varA <= $varB){
            $ganjil = '';
            $genap = '';
            
            for($i = $varA; $i <= $varB; $i++){
                if($i % 2 === 0){
               array_push($results, 'Angka ' . $i . ' Adalah Genap' . "<br>");
                }else if($i % 2 === 1){
                    array_push($results,'Angka ' . $i . ' Adalah Ganjil' . "<br>");
                }
              
            }
            return $results;
        }


    }
}
