<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GanjilGenapModel;


class GanjilGenapController extends Controller
{
    public function index(Request $request){
        $data = [];

        return view('ganjilgenap', compact('data'));
    }
    public function setProses(Request $request){
        $validatedData = $request->validate([
            'awal' => 'required',
            'akhir' => 'required',
        ]);
        if($validatedData) {
            $dataAwal =  $request->awal;
            $dataAkhir =  $request->akhir;
            $data = GanjilGenapModel::check($dataAwal, $dataAkhir);
        }
        return view('ganjilgenap', compact('data'));

    }
 
}
