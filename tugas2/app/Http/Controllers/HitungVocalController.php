<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VocalsModel;


class HitungVocalController extends Controller
{
    public function index(Request $request){

        $name = null;
        $result = [];

        return view('hitungvocal',compact('name', 'result'));
    }
   
 public function setProses(Request $request){
    $this->validate($request, [
        'name' => 'required|string',
    ]);
    $name = $request->name;
    $result = VocalsModel::setCountVocal($request->name);
    return view('hitungvocal', compact('name', 'result'));

 }
}
