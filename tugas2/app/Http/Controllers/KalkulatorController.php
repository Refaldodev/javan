<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KalkulatorController extends Controller
{
    public function index(Request  $request){
        
        $operation = $request->operation;
        $inputOne = $request->valueOne;
        $inputTwo = $request->valueTwo;
        $result = 0;

        if ($operation == "+") {
            $result = $inputOne + $inputTwo;
        } else if ($operation == "-") {
            $result = $inputOne - $inputTwo;
        } else if ($operation == "*") {
            $result = $inputOne * $inputTwo;
        } else if ($operation == "/") {
            if ($inputTwo != 0) {
                $result = $inputOne / $inputTwo;
            } else {
                echo "tidak bisa dilakukan";
            }
        }

        return view('kalkulator', compact('result', 'inputOne', 'inputTwo'));

    }
}
